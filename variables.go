package lg

var (
	Plugin       = Tag("plugin")
	Oper         = Tag("oper")
	Withdraw     = Tag("withdraw")
	Delta    Tag = "delta"
	Csc      Tag = "csc"
	Balance  Tag = "balance"
	Aml      Tag = "aml"
)

type Tag string

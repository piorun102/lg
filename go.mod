module gitlab.com/piorun102/lg

go 1.22.0

toolchain go1.22.2

require (
	github.com/google/uuid v1.6.0
	github.com/mattn/go-colorable v0.1.13
	github.com/opentracing/opentracing-go v1.2.0
	github.com/pkg/errors v0.9.1
	github.com/rogpeppe/go-internal v1.12.0
)

require (
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pmezard/go-difflib v1.0.1-0.20181226105442-5d4384ee4fb2 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
	golang.org/x/mod v0.21.0 // indirect
	golang.org/x/sys v0.25.0 // indirect
)

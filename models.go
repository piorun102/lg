package lg

import (
	"context"
	tracing "github.com/opentracing/opentracing-go"
	"gitlab.com/piorun102/lg/internal/zerolog"
	"net/http"
	"sync"
)

type lg struct {
	zl      *zerolog.Logger
	logs    *logsChan
	wgLogs  sync.WaitGroup
	wgFlush sync.WaitGroup
	mu      sync.Mutex
	stopCh  chan struct{}
}

type logsChan struct {
	logs chan []byte
}

func (lc *logsChan) Write(p []byte) (n int, err error) {
	cp := make([]byte, len(p))
	copy(cp, p)

	lc.logs <- cp
	return len(cp), nil
}

type lgCtx struct {
	empty  bool
	zl     *zerolog.Logger
	logs   *logsList
	mu     sync.Mutex
	header http.Header
	span   tracing.Span
	wg     sync.WaitGroup
	tags   map[string]struct{}
	context.Context
}
type TGLog struct {
	Service string `json:"service"`
	Tag     Tag    `json:"tag"`
	Msg     string `json:"msg"`
}
type logsList struct {
	logs [][]byte
}

func (ll *logsList) Write(p []byte) (n int, err error) {
	cp := make([]byte, len(p))
	copy(cp, p)

	ll.logs = append(ll.logs, cp)
	return len(p), nil
}

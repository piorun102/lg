package utils

import (
	"fmt"
	"github.com/pkg/errors"
	"github.com/rogpeppe/go-internal/modfile"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"sync"
)

func GetMarshalStack(frameAmount int) func(err error) interface{} {
	return func(err error) interface{} {
		type stackTracer interface {
			StackTrace() errors.StackTrace
		}
		sterr, ok := err.(stackTracer)
		if !ok {
			return nil
		}

		st := sterr.StackTrace()
		out := make([]map[string]string, 0, 3)

		wd, err := os.Getwd()
		if err != nil {
			log.Println(err)
		}

		flag := true
		for i := 1; i < min(frameAmount+1, len(st)-1); i++ {
			pwd, line, funcName := stackInfo(st[i])
			localPath, _ := filepath.Rel(wd, pwd)
			if strings.Contains(localPath, "src/runtime") {
				break
			}

			if flag && strings.Contains(localPath, "logger.go") {
				flag = false
				continue
			}

			source := fmt.Sprintf("%s:%s", localPath, line)

			out = append(out, map[string]string{
				"source": source,
				"func":   funcName,
			})
		}
		return out
	}
}

func Caller() string {
	pcc, file, line, ok := runtime.Caller(2)
	if !ok {
		log.Println("Failed to get caller information")
		return "unknown"
	}

	wd, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}

	localPath, _ := filepath.Rel(wd, file)

	funcName := runtime.FuncForPC(pcc).Name()
	return fmt.Sprintf("%s:%s:%d", localPath, strings.Split(funcName, ".")[1], line)
}

func stackInfo(f errors.Frame) (string, string, string) {
	fn := runtime.FuncForPC(pc(f))
	if fn == nil {
		return "unknown", "unknown", "unknown"
	}
	pwd, line := fn.FileLine(pc(f))
	funcName := funcname(fn.Name())

	return pwd, strconv.Itoa(line), funcName
}

func funcname(name string) string {
	i := strings.LastIndex(name, "/")
	name = name[i+1:]
	i = strings.Index(name, ".")
	return name[i+1:]
}

func pc(f errors.Frame) uintptr { return uintptr(f) - 1 }

var once sync.Once
var serviceName string

func GetServiceName() string {
	once.Do(func() {
		goModPath := "go.mod"

		content, err := os.ReadFile(goModPath)
		if err != nil {
			log.Println("Unable to read go.mod: ", err)
			serviceName = "empty"
			return
		}

		modFile, err := modfile.Parse("go.mod", content, nil)
		if err != nil {
			log.Println("Unable to parse go.mod: ", err)
			serviceName = "empty"
			return
		}

		serviceName = modFile.Module.Mod.Path
	})

	return serviceName
}

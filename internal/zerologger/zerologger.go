package zerologger

import (
	"gitlab.com/piorun102/lg/internal/utils"
	"gitlab.com/piorun102/lg/internal/zerolog"
	"log"
	"os"
	"sync"
	"time"
)

var zl *zerolog.Logger
var consoleWriterPtr *zerolog.ConsoleWriter
var once sync.Once

func GetZeroLogger() (*zerolog.Logger, *zerolog.ConsoleWriter) {
	once.Do(func() {
		moscowLocation, err := time.LoadLocation("Europe/Moscow")
		if err != nil {
			log.Println("Failed to load Moscow timezone")
		}

		zerolog.ErrorStackMarshaler = utils.GetMarshalStack(3)
		zerolog.TimeFieldFormat = time.StampMilli
		zerolog.ErrorFieldName = zerolog.MessageFieldName
		zerolog.TimestampFunc = func() time.Time {
			return time.Now().In(moscowLocation)
		}

		consoleWriterPtr = &zerolog.ConsoleWriter{Out: os.Stderr,
			TimeFormat:    time.TimeOnly,
			FieldsExclude: []string{"stack"},
			NoColor:       false,
		}

		logger := zerolog.New(consoleWriterPtr).
			With().
			Timestamp().
			Caller().
			Stack().
			Logger()

		zl = &logger
	})

	return zl, consoleWriterPtr
}

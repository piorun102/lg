package lg

import (
	"bytes"
	"encoding/json"
	"github.com/pkg/errors"
	"gitlab.com/piorun102/lg/internal"
	"gitlab.com/piorun102/lg/internal/utils"
	"gitlab.com/piorun102/lg/internal/zerolog"
	"gitlab.com/piorun102/lg/internal/zerologger"
	"io"
	"net/http"
	"net/url"
	"os"
	"sync"
	"time"
)

var logger *lg
var once sync.Once

func init() {
	Get()
}

func Get() Logger {
	once.Do(func() {
		zl, consoleWriterPtr := zerologger.GetZeroLogger()

		newLogger := zl.Output(consoleWriterPtr)

		logger = &lg{zl: &newLogger, stopCh: make(chan struct{})}
	})

	return logger
}
func ConnectTg(address string) (err error) {
	var u *url.URL
	u, err = url.Parse(address)
	if err != nil {
		return errors.Wrap(err, "url parse error")
	}
	internal.Tg = u.String()
	return
}

var tgFC = 2

func Tg(message string, tag Tag) {
	if internal.Tg == "" {
		ErrorfFC(tgFC, "empty tg url can't send message %v with tag %v", message, tag)
		return
	}
	if message == "" {
		ErrorfFC(tgFC, "empty message %v with tag %v", message, tag)
		return
	}
	if tag == "" {
		ErrorfFC(tgFC, "empty tag %v with message %v", tag, message)

		return
	}
	data := TGLog{
		Service: utils.GetServiceName(),
		Tag:     tag,
		Msg:     message,
	}
	jsonData, err := json.Marshal(data)
	if err != nil {
		return
	}
	DebugfFC(tgFC, "[TG][%v]%v", tag, message)
	go func() {
		var r *http.Response

		r, err = http.Post(internal.Tg, "application/json", bytes.NewBuffer(jsonData))
		if err != nil {
			ErrorfFC(tgFC, "failed to send log message with error %v", err)
			return
		}
		if r.StatusCode != http.StatusOK {
			ErrorfFC(tgFC, "failed to send log message with status code %v", r.StatusCode)
			return
		}
	}()
	return
}
func Connect(address string) {
	internal.Addr = address

	zl, consoleWriterPtr := zerologger.GetZeroLogger()
	chanWriter := logsChan{logs: make(chan []byte, 10000)}

	writers := []io.Writer{
		zerolog.SyncWriter(&chanWriter),
		consoleWriterPtr,
	}

	multiWriter := zerolog.MultiLevelWriter(writers...)
	newLogger := zl.Output(multiWriter)

	l := &lg{zl: &newLogger, logs: &chanWriter, stopCh: make(chan struct{})}

	l.wgFlush.Add(1)
	go func() {
		defer l.wgFlush.Done()
		l.StartLogFlusher()
	}()

	logger = l
}

func (l *lg) Send(n int) {
	if n == 0 {
		return
	}

	l.wgLogs.Add(1)
	defer l.wgLogs.Done()
	l.realSend(n)
}

func (l *lg) realSend(n int) {
	logs := make([][]byte, n, n)
	for i := 0; i < n; i++ {
		logs[i] = <-l.logs.logs
	}

	jsonLogs, err := json.Marshal(logs)
	if err != nil {
		l.zl.Error().Msgf("Error marshalling logs to JSON: %v", err)
		return
	}

	req, err := http.NewRequest("POST", internal.Addr, bytes.NewBuffer(jsonLogs))
	if err != nil {
		l.zl.Error().Msgf("Error creating request: %v", err)
		return
	}

	req.Header.Add("serviceId", utils.GetServiceName())

	client := &http.Client{}
	_, err = client.Do(req)
	if err != nil {
		l.zl.Error().Msgf("Error making request: %v", err)
		return
	}
}
func TrancefFC(fc int, msg string, args ...any) {
	logger.wgLogs.Add(1)
	logger.zl.Trace().CallerSkipFrame(fc).Done(func(msg string) {
		logger.wgLogs.Done()
	}).Msgf(msg, args...)
}
func (l *lg) Tracef(msg string, args ...any) {
	l.realTracef(msg, args...)
}

func (l *lg) realTracef(msg string, args ...any) {
	l.wgLogs.Add(1)
	l.zl.Trace().CallerSkipFrame(2).Done(func(msg string) {
		l.wgLogs.Done()
	}).Msgf(msg, args...)
}
func DebugfFC(fc int, msg string, args ...any) {
	logger.wgLogs.Add(1)
	logger.zl.Debug().CallerSkipFrame(fc).Done(func(msg string) {
		logger.wgLogs.Done()
	}).Msgf(msg, args...)
}
func (l *lg) Debugf(msg string, args ...any) {
	l.realDebugf(msg, args...)
}

func (l *lg) realDebugf(msg string, args ...any) {
	l.wgLogs.Add(1)
	l.zl.Debug().CallerSkipFrame(2).Done(func(msg string) {
		l.wgLogs.Done()
	}).Msgf(msg, args...)
}
func InfofFC(fc int, msg string, args ...any) {
	logger.wgLogs.Add(1)
	logger.zl.Info().CallerSkipFrame(fc).Done(func(msg string) {
		logger.wgLogs.Done()
	}).Msgf(msg, args...)
}
func (l *lg) Infof(msg string, args ...any) {
	l.realInfof(msg, args...)
}

func (l *lg) realInfof(msg string, args ...any) {
	l.wgLogs.Add(1)
	l.zl.Info().CallerSkipFrame(2).Done(func(msg string) {
		l.wgLogs.Done()
	}).Msgf(msg, args...)
}
func WarnfFC(fc int, msg string, args ...any) {
	logger.wgLogs.Add(1)
	logger.zl.Warn().CallerSkipFrame(fc).Done(func(msg string) {
		logger.wgLogs.Done()
	}).Msgf(msg, args...)
}
func (l *lg) Warnf(msg string, args ...any) {
	l.realWarnf(msg, args...)
}

func (l *lg) realWarnf(msg string, args ...any) {
	l.wgLogs.Add(1)
	l.zl.Warn().CallerSkipFrame(2).Done(func(msg string) {
		l.wgLogs.Done()
	}).Err(errors.Errorf(msg, args...)).Send()
}
func ErrorfFC(fc int, msg string, args ...any) {
	logger.wgLogs.Add(1)
	logger.zl.Error().CallerSkipFrame(fc).Done(func(msg string) {
		logger.wgLogs.Done()
	}).Msgf(msg, args...)
}
func (l *lg) Errorf(msg string, args ...any) {
	l.realErrorf(msg, args...)
}

func (l *lg) Error(msg string) {
	l.realErrorf(msg)
}

func (l *lg) realErrorf(msg string, args ...any) {
	l.wgLogs.Add(1)
	l.zl.Error().CallerSkipFrame(2).Done(func(msg string) {
		l.wgLogs.Done()
	}).Err(errors.Errorf(msg, args...)).Send()
}

func (l *lg) Fatalf(msg string, args ...any) {
	l.realFatalf(msg, args...)
}

func (l *lg) realFatalf(msg string, args ...any) {
	l.wgLogs.Add(1)
	l.zl.WithLevel(zerolog.FatalLevel).CallerSkipFrame(2).Done(func(msg string) {
		l.wgLogs.Done()
	}).Err(errors.Errorf(msg, args...)).Send()

	l.wgLogs.Wait()
	close(l.stopCh)
	l.wgFlush.Wait()
	os.Exit(1)
}

func (l *lg) Panicf(msg string, args ...any) {
	l.realPanicf(msg, args...)
}

func (l *lg) realPanicf(msg string, args ...any) {
	l.wgLogs.Add(1)
	l.zl.WithLevel(zerolog.PanicLevel).CallerSkipFrame(2).Done(func(msg string) {
		l.wgLogs.Done()
	}).Err(errors.Errorf(msg, args...)).Send()

	l.wgLogs.Wait()
	close(l.stopCh)
	l.wgFlush.Wait()
	panic(msg)
}

func (l *lg) StartLogFlusher() {
	timeout := 1 * time.Second

	for {
		select {
		case <-time.After(timeout):
			l.Send(len(l.logs.logs))
		case <-l.stopCh:
			l.Send(len(l.logs.logs))
			return
		}
	}
}

// Print same as Println
func Print(msg string) {
	logger.realDebugf(msg)
}

func Printf(format string, args ...any) {
	logger.realDebugf(format, args...)
}

func Println(msg string) {
	logger.realDebugf(msg)
}

func Trace(msg string) {
	logger.realTracef(msg)
}

func Debug(msg string) {
	logger.realDebugf(msg)
}

func Info(msg string) {
	logger.realInfof(msg)
}

func Warn(msg string) {
	logger.realWarnf(msg)
}

func Error(err error) {
	if err != nil {
		logger.realErrorf(err.Error())
	}
}

func Fatal(msg string) {
	logger.realFatalf(msg)
}

func Panic(msg string) {
	logger.realPanicf(msg)
}

func Tracef(format string, args ...any) {
	logger.realTracef(format, args...)
}

func Debugf(format string, args ...any) {
	logger.realDebugf(format, args...)
}

func Infof(format string, args ...any) {
	logger.realInfof(format, args...)
}

func Warnf(format string, args ...any) {
	logger.realWarnf(format, args...)
}

func Errorf(format string, args ...any) {
	logger.realErrorf(format, args...)
}

func Fatalf(format string, args ...any) {
	logger.realFatalf(format, args...)
}

func Panicf(format string, args ...any) {
	logger.realPanicf(format, args...)
}

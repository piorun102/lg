package lg

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	tracing "github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"gitlab.com/piorun102/lg/internal"
	"gitlab.com/piorun102/lg/internal/utils"
	"gitlab.com/piorun102/lg/internal/zerolog"
	"gitlab.com/piorun102/lg/internal/zerologger"
	"io"
	"net/http"
	"os"
	"strings"
)

func Ctx(ctx context.Context, header map[string][]string) CtxLogger {
	zl, consoleWriterPtr := zerologger.GetZeroLogger()
	span := tracing.SpanFromContext(ctx)
	listWriter := logsList{
		logs: make([][]byte, 0),
	}

	writers := []io.Writer{
		zerolog.SyncWriter(&listWriter),
		consoleWriterPtr,
	}

	multiWriter := zerolog.MultiLevelWriter(writers...)
	newLogger := zl.Output(multiWriter)

	if header == nil {
		header = map[string][]string{}
	}
	h := http.Header(header)
	h.Add("serviceId", utils.GetServiceName())
	h.Add("requestId", uuid.NewString())

	lCtx := &lgCtx{
		zl:      &newLogger,
		empty:   false,
		logs:    &listWriter,
		header:  h,
		tags:    make(map[string]struct{}),
		span:    span,
		Context: ctx,
	}
	if span == nil {
		lCtx.zl.Warn().CallerSkipFrame(1).Msg("Unable to extract span from ctx")
	}

	return lCtx
}
func N(operation string) CtxLogger {
	sp, c := tracing.StartSpanFromContext(context.Background(), operation)
	zl, consoleWriterPtr := zerologger.GetZeroLogger()
	listWriter := logsList{
		logs: make([][]byte, 0),
	}

	writers := []io.Writer{
		zerolog.SyncWriter(&listWriter),
		consoleWriterPtr,
	}

	multiWriter := zerolog.MultiLevelWriter(writers...)
	newLogger := zl.Output(multiWriter)

	h := http.Header{}
	h.Add("serviceId", utils.GetServiceName())
	h.Add("requestId", uuid.NewString())

	eCtx := &lgCtx{
		zl:      &newLogger,
		empty:   false,
		logs:    &listWriter,
		header:  h,
		tags:    make(map[string]struct{}),
		Context: c,
		span:    sp,
	}
	return eCtx
}
func EmptyCtx() CtxLogger {
	sp, c := tracing.StartSpanFromContext(context.Background(), "empty")
	zl, consoleWriterPtr := zerologger.GetZeroLogger()
	listWriter := logsList{
		logs: make([][]byte, 0),
	}

	writers := []io.Writer{
		zerolog.SyncWriter(&listWriter),
		consoleWriterPtr,
	}

	multiWriter := zerolog.MultiLevelWriter(writers...)
	newLogger := zl.Output(multiWriter)

	h := http.Header{}
	h.Add("serviceId", utils.GetServiceName())
	h.Add("requestId", uuid.NewString())

	eCtx := &lgCtx{
		zl:      &newLogger,
		empty:   false,
		logs:    &listWriter,
		header:  h,
		tags:    make(map[string]struct{}),
		Context: c,
		span:    sp,
	}
	return eCtx
}

func (l *lgCtx) Ctx() context.Context {
	return l.Context
}

func (l *lgCtx) AddTags(tags ...string) {
	l.mu.Lock()
	defer l.mu.Unlock()
	for _, tag := range tags {
		l.tags[tag] = struct{}{}
	}
}

func (l *lgCtx) SpanLog(topic, format string, args ...any) {
	if l.span != nil && !l.empty {
		l.span.LogKV(topic, fmt.Sprintf(format, args...))
	}
}

func (l *lgCtx) SetHeader(header map[string][]string) {
	if header == nil {
		l.Warnf("Header is nil")
	}
	l.header = header
}

func (l *lgCtx) Connect(addr string) {
	internal.Addr = addr
}

func (l *lgCtx) Send() {
	go func() {
		l.wg.Wait()
		l.realSend()
	}()
}
func (l *lgCtx) End(err *error) {
	if *err != nil {
		l.wg.Add(1)
		l.zl.Error().CallerSkipFrame(1).Err(errors.Errorf("%+v", *err)).Done(func(msg string) {
			l.wg.Done()
		}).Send()
	}
	if l.span != nil {
		l.span.Finish()
	}
	go func() {
		l.wg.Wait()
		l.realSend()
	}()
}

func (l *lgCtx) realSend() {
	l.header["tag"] = []string{}
	for key := range l.tags {
		l.header["tag"] = append(l.header["tag"], key)
		if l.span != nil {
			l.span.SetTag(key, true)
		}
	}

	jsonLogs, err := json.Marshal(l.logs.logs)
	if err != nil {
		l.zl.Error().Msgf("Error marshalling logs to JSON: %v", err)
		return
	}

	req, err := http.NewRequest("POST", internal.Addr, bytes.NewBuffer(jsonLogs))
	if err != nil {
		l.zl.Error().Msgf("Error creating request: %v", err)
		return
	}

	req.Header = l.header

	client := &http.Client{}
	_, err = client.Do(req)
	if err != nil {
		if strings.Contains(err.Error(), "Post \"\"") {
			l.zl.Warn().Msgf("Error making request: URL == \"\". Use lg.Connect(url)")
			return
		}
		l.zl.Error().Msgf("Error making request: %v", err)
		return
	}

	l.logs.logs = l.logs.logs[:0]
}

func (l *lgCtx) Tracef(msg string, args ...any) {
	l.wg.Add(1)
	l.zl.Trace().CallerSkipFrame(1).Done(func(msg string) {
		l.wg.Done()
	}).Msgf(msg, args...)
}
func (l *lgCtx) TracefFC(fc int, msg string, args ...any) {
	l.wg.Add(1)
	l.zl.Trace().CallerSkipFrame(fc).Done(func(msg string) {
		l.wg.Done()
	}).Msgf(msg, args...)
}
func (l *lgCtx) Debugf(msg string, args ...any) {
	l.wg.Add(1)
	l.zl.Debug().CallerSkipFrame(1).Done(func(msg string) {
		l.wg.Done()
	}).Msgf(msg, args...)
}

func (l *lgCtx) Infof(msg string, args ...any) {
	l.wg.Add(1)
	l.zl.Info().CallerSkipFrame(1).Done(func(msg string) {
		l.wg.Done()
	}).Msgf(msg, args...)
}

func (l *lgCtx) Warnf(msg string, args ...any) {
	l.wg.Add(1)
	l.zl.Warn().CallerSkipFrame(1).Err(errors.Errorf(msg, args...)).Done(func(msg string) {
		l.wg.Done()
	}).Send()
}

func (l *lgCtx) Errorf(msg string, args ...any) {
	l.wg.Add(1)
	l.zl.Error().CallerSkipFrame(1).Err(errors.Errorf(msg, args...)).Done(func(msg string) {
		l.wg.Done()
	}).Send()
}

func (l *lgCtx) Error(err error) {
	if err != nil {
		l.wg.Add(1)
		l.zl.Error().CallerSkipFrame(1).Err(errors.Errorf(err.Error())).Done(func(msg string) {
			l.wg.Done()
		}).Send()
	}
}

func (l *lgCtx) DError(msg string) {
	l.wg.Add(1)
	l.zl.Error().CallerSkipFrame(2).Err(errors.Errorf(msg)).Done(func(msg string) {
		l.wg.Done()
	}).Send()
}

func (l *lgCtx) DefError(err *error) {
	if *err != nil {
		l.wg.Add(1)
		l.zl.Error().CallerSkipFrame(1).Err(*err).Done(func(msg string) {
			l.wg.Done()
		}).Send()
	}
}
func (l *lgCtx) Fatalf(msg string, args ...any) {
	l.wg.Add(1)
	l.zl.WithLevel(zerolog.FatalLevel).CallerSkipFrame(1).Err(errors.Errorf(msg, args...)).Done(func(msg string) {
		l.wg.Done()
	}).Send()

	l.Send()
	os.Exit(1)
}

func (l *lgCtx) Panicf(msg string, args ...any) {
	l.wg.Add(1)
	l.zl.WithLevel(zerolog.PanicLevel).CallerSkipFrame(1).Err(errors.Errorf(msg, args...)).Done(func(msg string) {
		l.wg.Done()
	}).Send()

	l.Send()
	panic(msg)
}
